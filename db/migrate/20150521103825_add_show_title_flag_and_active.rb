class AddShowTitleFlagAndActive < ActiveRecord::Migration
	def change
		add_column :rhinobook_book_translations, :active, :boolean, default: true

		add_column :rhinobook_books, :show_name, :boolean, default: false
		add_column :rhinobook_book_translations, :show_name, :boolean, default: true

		add_index :rhinobook_books, :show_name
	end
end
