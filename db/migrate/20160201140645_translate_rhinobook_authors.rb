class TranslateRhinobookAuthors < ActiveRecord::Migration
	def self.up
		Rhinobook::Author.create_translation_table!({
			name: {:type => :string, :null => false},
			email: {:type => :string},
			web_site: {:type => :string},
		}, {
			migrate_data: true
		})

	    add_column :rhinobook_author_translations, :active, :boolean, default: true
	    add_index  :rhinobook_author_translations, :name
	    add_index  :rhinobook_author_translations, :email
	    add_index  :rhinobook_author_translations, :active
	end

	def self.down
		Rhinobook::Author.drop_translation_table! :migrate_data => true
	end
end
