class CreateRhinobookLibraries < ActiveRecord::Migration
	def change
		create_table :rhinobook_libraries do |t|
			t.string :name, :null => false
			t.string :slug, :null => false
			t.string :domain_name
			t.text :descr
			t.boolean :active, :default => true

			t.timestamps null: false
		end
		add_index :rhinobook_libraries, :name, :unique => true 
		add_index :rhinobook_libraries, :slug, :unique => true 
		add_index :rhinobook_libraries, :domain_name, :unique => true 

		create_table :rhinobook_libraries_books do |t|
	        t.references :library
	        t.references :book
		end
		add_index :rhinobook_libraries_books, [:library_id, :book_id], :unique => true 
		add_index :rhinobook_libraries_books, :book_id
	end
end
