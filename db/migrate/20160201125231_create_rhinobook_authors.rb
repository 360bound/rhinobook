class CreateRhinobookAuthors < ActiveRecord::Migration
	def change
		create_table :rhinobook_authors do |t|
			t.references :rhinobook_books, index: true

			t.string :name
			t.string :email
			t.string :web_site
			t.text :info
			t.boolean :active, default: true

			t.timestamps null: false		
		end

		add_index :rhinobook_authors, :name
		add_index :rhinobook_authors, :email
		add_index :rhinobook_authors, :active
		add_index :rhinobook_authors, [:name, :email], :unique => true

		rename_column :rhinobook_books, :author, :author_name
	end
end
