class AddAuthTokenToUser < ActiveRecord::Migration

  def self.up
    Rhinoart::User.reset_column_information
    return if Rhinoart::User.column_names.include? 'auth_token'
    add_column :rhinoart_users, :auth_token, :string
    add_index  :rhinoart_users, :auth_token, :unique => true
  end

  def self.down
    remove_column :rhinoart_users, :auth_token
  end

end
