class AddVersionFieldToRhinobookBooks < ActiveRecord::Migration
  def change
    add_column :rhinobook_books, :version, :string
  end
end
