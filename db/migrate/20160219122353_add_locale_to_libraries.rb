class AddLocaleToLibraries < ActiveRecord::Migration
	def change
		add_column :rhinobook_libraries, :locale, :string
		add_index :rhinobook_libraries, :locale

		remove_index :rhinobook_libraries, :name
		remove_index :rhinobook_libraries, :slug

		add_index :rhinobook_libraries, :name
		add_index :rhinobook_libraries, [:slug, :locale], unique: true 
	end
end
