module Rhinobook
  module ApiHelper

    def format_date(date)
      date.try(:strftime, '%Y-%m-%d %H:%M')
    end

  end
end