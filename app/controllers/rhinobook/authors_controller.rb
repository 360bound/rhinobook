require_dependency "rhinobook/application_controller"

module Rhinobook
	class AuthorsController < BaseController
		before_action :set_book, only: [:index, :new, :create, :edit, :update, :destroy]
		before_action :set_author, only: [:edit, :update, :destroy]

		def index			
			@authors = @book.authors
		end

		def new
			@author = Author.new
		end

		def create
			@author = Author.new(author_params)
			@author.book = @book

			if @author.save
				redirect_to params[:redirect_to] || [@book, :authors]
			else
				render action: 'new'
			end			
		end

		def edit
		end

		def update
			if @author.update(author_params)
				redirect_to params[:redirect_to] || [@book, :authors]
			else
				render action: 'edit'
			end
		end

		def destroy
			@author.destroy
			redirect_to params[:redirect_to] || [@book, :authors]
		end

		private
			def set_book
				@book = Book.find params[:book_id]	
			rescue Exception => e
				render :template => 'site/not_found', :status => 404
			end

			def set_author
				@author = Author.find params[:id]	
			rescue Exception => e
				render :template => 'site/not_found', :status => 404				
			end

			def author_params
				params[:author].permit!
			end
	end
end
