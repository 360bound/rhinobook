module Rhinobook
  module Api
    class PagesController < BaseController

      swagger_controller :books, "Pages"

      swagger_model :Page do
        description "Page Structure"
        property :id, :integer, :required, "Page Id", :defaultValue => 5
        property :book_id, :integer, :required, "Chapter Id", :defaultValue => 2
        property :number, :integer, :required, "Name", :defaultValue => 1
        property :content, :string, :required, "Page Content", :defaultValue => "Page Content"
        property :advice, :integer, :required, "Advice", :defaultValue => 0
        property :resume, :integer, :required, "Resume", :defaultValue => 0
      end

      swagger_error_model

      swagger_api :index do
        summary "Fetches Pages for Chapter"
        notes "This lists Chapter Pages by chapter_id"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :chapter_id, :integer, :required, "Chapter ID"
        response :ok, 'Success', :Page
        response :unauthorized, nil, :Error
        response :not_found, nil, :Error
      end

      def index
        @chapter = Chapter.find(params[:chapter_id])
        @pages = @chapter.pages
      end

      swagger_api :show do
        summary "Fetches Page Data by Id"
        notes "Page Data"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :id, :integer, :required, "Page Id"
        response :ok, 'Success', :Page
        response :unauthorized, nil, :Error
        response :not_found, nil, :Error
      end

      def show
        @page = Page.find(params[:id])
      end

    end
  end
end
