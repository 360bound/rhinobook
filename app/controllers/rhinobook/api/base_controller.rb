module Rhinobook
  module Api
    class BaseController < ApplicationController

      before_filter :has_api_access
      before_filter :set_locale

      class InvalidAccessRightError < StandardError; end
      class InvalidApiKeyError < StandardError; end
      class InvalidParametersError < StandardError; end
      class UserNotApprovedError < StandardError; end
      class NotFoundError < StandardError; end
      class DomainNotFoundError < StandardError; end
      class InvalidEmailError < StandardError; end
      class InvalidPasswordError < StandardError; end
      class ResourceNotFoundError < StandardError; end

      rescue_from(InvalidAccessRightError) { render_error(nil, Api::ERROR_ACCESS_DENIED_USER_HAS_NO_ACCESS_RIGHT, :unauthorized) }
      rescue_from(InvalidApiKeyError){ render_error(nil, Api::ERROR_ACCESS_DENIED_INVALID_API_TOKEN, :unauthorized) }
      rescue_from(InvalidParametersError){ render_error(nil, Api::ERROR_PARAM_TYPE, :not_found) }
      rescue_from(UserNotApprovedError){ render_error(nil, Api::ERROR_ACCESS_DENIED_USER_WASNT_APPROVED, :unauthorized) }
      rescue_from(NotFoundError){ render_error(nil, Api::ERROR_CONTENT_NOT_FOUND, :not_found) }
      rescue_from(DomainNotFoundError){ render_error(nil, Api::ERROR_DOMAIN_NOT_FOUND, :unprocessable_entity) }
      rescue_from(InvalidPasswordError){ render_error(nil, Api::ERROR_PASSWORD_MISMATCH, :forbidden) }
      rescue_from(ResourceNotFoundError){ render_error(nil, Api::ERROR_RESOURCE_NOT_FOUND, :not_found) }
      rescue_from(InvalidEmailError){ render_error(nil, Api::ERROR_PASSWORD_MISMATCH, :unauthorized) }
      rescue_from(InvalidPasswordError){ render_error(nil, Api::ERROR_PASSWORD_MISMATCH, :unauthorized) }
      rescue_from(ActiveRecord::RecordNotFound){ render_error(nil, Api::ERROR_RESOURCE_NOT_FOUND, :not_found) }
      rescue_from(NoMethodError){|e| render_error(e.message, Api::ERROR_SERVER, :internal_server_error) }


      private

      def render_error(message, code, status, options = {})
        message ||= t("api_errors.#{code}")
        render json: {message: message, error_code: code}.merge!(options), status: status
      end

      def set_locale
        I18n.locale = params[:locale] || :en
      end

      def has_api_access

        # @api_user = Rhinoart::User.find_by(auth_token: params[:api_token], approved: true) if params[:api_token].present?
        #
        # raise InvalidApiKeyError.new if @api_user.nil?
        # raise UserNotApprovedError.new unless @api_user.approved
        # raise InvalidAccessRightError.new if !@api_user.has_access_to_api?
        #TODO: temporarily
        return true

      end

      def self.swagger_error_model
        swagger_model :Error do
          description "Error response"
          property :message, :string, :required, "Error Message", {defaultValue: 'Error'}
          property :error_code, :string, :required, "Error Code", {defaultValue: 'Code'}
        end
      end

    end
  end
end