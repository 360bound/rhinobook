module Rhinobook
  module Api
    class ChaptersController< Api::BaseController

      swagger_controller :books, "Chapters"

      swagger_model :Chapter do
        description "Chapter Structure"
        property :id, :integer, :required, "Chapter Id", :defaultValue => 5
        property :book_id, :integer, :required, "Book Id", :defaultValue => 2
        property :name, :string, :required, "Name", :defaultValue => 'Chapter Name'
        property :position, :integer, :required, "Position Number in Book", :defaultValue => 1
      end

      swagger_error_model

      swagger_api :index do
        summary "Fetches Chapters for Book"
        notes "This lists Book Chapters by book_id"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :book_id, :integer, :required, "Book ID"
        response :ok, 'Success', :Chapter
        response :unauthorized, nil, :Error
        response :not_found, nil, :Error
      end

      def index
        @book = Book.find params[:book_id]
        @chapters = @book.chapters
      end

      swagger_api :show do
        summary "Fetches Chapter Data by Id"
        notes "Chapter Data"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :id, :integer, :required, "Chapter Id"
        response :ok, 'Success', :Chapter
        response :unauthorized, nil, :Error
        response :not_found, nil, :Error
      end

      def show
        @chapter = Chapter.find(params[:id])
      end

    end
  end
end
