module Rhinobook
  module Api
    class BooksController < Api::BaseController

      swagger_controller :books, "Books"


      swagger_model :Book do
        description "Book Structure"
        property :id, :integer, :required, "Book Id"
        property :name, :string, :required, "Name"
        property :descr, :string, :required, "Description"
        property :author, :string, :required, "Author"
        property :created_at, :string, :required, "Created Date"
        property :updated_at, :string, :required, "Last modified Date"
        property :image, :Image, :required, "Image"
        property :thumb_image, :Image, :required, "Thumb Image"
        property :documents_file_size, :integer, :required, "Image files size"
      end

      swagger_model :Image do
        description "Books Image"
        property :url, :string, :required, "Image Path"
        property :file_size, :integer, :required, "Image File Size"
        property :content_type, :integer, :required, "Content Type"
      end

      swagger_error_model

      swagger_api :index do
        summary "Fetches all Books items"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        response :ok, 'Success', :Book
        response :unauthorized, nil, :Error
      end

      def index
        @books = Rhinobook::Book.all
      end

      swagger_api :show do
        summary "Fetches Book Data by Id"
        notes "Book Data"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :id, :integer, :required, "Book Id"
        response :unauthorized, nil, :Error
        response :not_found, nil, :Error
      end

      def show
        @book = Rhinobook::Book.includes(:chapters => [{:pages => [:images, :videos]}]).find(params[:id])
        #render json: @book.as_json(:include => {chapters: {include: {pages: {include: [:videos, :images]}}}})
      end

      swagger_api :version do
        summary "return actual version of Book by Id"
        notes "Book Version"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :id, :integer, :required, "Book Id"
        response :unauthorized, nil, :Error
        response :not_found, nil, :Error
      end

      def version
        @book = Rhinobook::Book.find(params[:id])
        render json: {:version => (@book.version.present? ? @book.version : '0')}
      end

    end
  end
end