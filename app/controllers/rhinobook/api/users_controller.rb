module Rhinobook
  module Api
    class UsersController < BaseController

      skip_before_action :has_api_access, only: [:create, :authorize]
      caches_action :authorize, :show, expires_in: 60.seconds, unless_exist: true
      #skip_before_filter  :verify_authenticity_token

      swagger_controller :users, "Users"

      swagger_model :User do
        description "User Structure"
        property :id, :integer, :required, "ID", :defaultValue => 1
        property :email, :string, :required, "Email", :defaultValue => 'test@mail.com'
        property :name, :string, :required, "Name", :defaultValue => 'User Name'
      end

      swagger_model :UserWithToken do
        description "User Structure"
        property :id, :integer, :required, "ID", :defaultValue => 1
        property :email, :string, :required, "Email", :defaultValue => 'test@mail.com'
        property :name, :string, :required, "Name", :defaultValue => 'User Name'
        property :api_token, :string, :optional, "Api Token", :defaultValue => 'User Name'
      end

      swagger_model :ValidationError do
        description "Error response"
        property :message, :string, :required, "Error Message", {defaultValue: 'Error'}
        property :error_code, :string, :required, "Error Code", {defaultValue: 'Code'}
        property :errors, :hash, :required, "Error Code", {defaultValue: {email: ['error']}}
      end

      swagger_api :create do
        summary "Create new user"
        #param :query, :locale, :integer, :optional, "Language locale"
        param :form, 'user[email]', :string, :required, "Email", :defaultValue => 'test@mail.com'
        param :form, 'user[name]', :string, :required, "Name", :defaultValue => 'User Name'
        param :form, 'user[password]', :string, :required, "Password", :defaultValue => "password"
        param :form, 'user[password_confirmation]', :string, :required, "Password confirmation", :defaultValue => "password"
        response :ok, 'Success', :UserWithToken
        response :unprocessable_entity, nil, :ValidationError
      end

      def create
        @user = Rhinoart::User.new(user_params)
        @user.add_role(Rhinoart::User::API_ROLE_USER) if !@user.has_role?(Rhinoart::User::API_ROLE_USER)
        render_error(nil, Api::ERROR_USER_VALIDATION_ERROR, :unprocessable_entity, errors: @user.errors) unless @user.save
      end

      swagger_api :authorize do
        summary "Authorize user. Returns API token if user was approved"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :form, 'user[email]', :string, :required, "Email", :defaultValue => 'test@mail.com'
        param :form, 'user[password]', :string, :required, "Password", :defaultValue => "password"
        response :ok, 'Success', :User
        response :unauthorized, nil, :Error
      end

      def authorize
        @user = Rhinoart::User.find_by(email: user_params[:email], approved: true)

        raise InvalidEmailError.new if @user.nil?
        raise InvalidPasswordError.new unless @user.valid_password? user_params[:password].to_s
        raise InvalidAccessRightError.new if !@user.has_access_to_api?
        @user.reset_auth_token! if @user.auth_token.blank?

        sign_in @user
      end

      swagger_api :show do
        summary "Show user data"
        param :query, :api_token, :string, :required, "Api Token"
        param_list :path, :locale, :string, :optional, "Language locale", ['en', 'ru'], :defaultValue => 'en'
        param :path, :id, :string, :required, "User Id"
        response :ok, 'Success', :User
        response :unprocessable_entity, nil, :Error
      end

      def show
        raise InvalidAccessRightError.new if params[:id].to_i != current_user.id
        @user = Rhinoart::User.find(params[:id])
      end

      private

      def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
      rescue Exception => e
        raise InvalidParametersError.new
      end

    end
  end
end