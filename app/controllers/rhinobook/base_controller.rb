require_dependency "rhinobook/application_controller"

module Rhinobook
	class BaseController < Rhinoart::BaseController
		before_action :set_locale
		before_action { authorize! :manage, :books }

		class InternalServerError < StandardError; end
		class NotFoundError < StandardError; end
		class AuthorizationError < StandardError; end

		
		rescue_from InternalServerError do |exception|
			render 'shared/error_500', status: 500
		end

		rescue_from NotFoundError do |exception|
			render 'shared/error_404', status: 404
		end

		rescue_from AuthorizationError do |exception|
			render 'shared/error_401', status: 401
		end

		def default_url_options(options={})
			if I18n.locale != I18n.default_locale
				{ locale: I18n.locale }
			else
				{ locale: nil }
			end
		end		
	end
end