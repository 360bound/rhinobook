# == Schema Information
#
# Table name: rhinobook_authors
#
#  id                 :integer          not null, primary key
#  rhinobook_books_id :integer
#  name               :string(255)
#  email              :string(255)
#  web_site           :string(255)
#  info               :text(65535)
#  active             :boolean          default(TRUE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module Rhinobook
	class Author < ActiveRecord::Base
		belongs_to :book, :foreign_key => "rhinobook_books_id"
		# belongs_to :image, as: :imageable, :dependent => :destroy

		has_many :images, as: :imageable, :dependent => :destroy
		accepts_nested_attributes_for :images, allow_destroy: true

		validates :name, presence: true
		validates_uniqueness_of [:name, :email], scope: :rhinobook_books_id

		validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, if: Proc.new { |a| !a.email.blank? }

		translates :name, :email, :web_site, :active, versioning: :paper_trail
		globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names	
	end
end
