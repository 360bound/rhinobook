# == Schema Information
#
# Table name: rhinobook_books
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  slug        :string(255)      not null
#  descr       :text(65535)
#  active      :boolean          default(FALSE)
#  created_at  :datetime
#  updated_at  :datetime
#  author_name :string(255)
#  image_ru    :string(255)
#  image_en    :string(255)
#  show_name   :boolean          default(FALSE)
#  version     :string(255)
#

module Rhinobook
	class Book < Rhinoart::Base
		# has_paper_trail
		before_validation :name_to_slug

		has_many :pages, :dependent => :destroy, :foreign_key => "rhinobook_books_id"
		accepts_nested_attributes_for :pages, allow_destroy: true, reject_if: :all_blank

		has_many :chapters, ->{ order(position: :asc) }, :dependent => :destroy, :foreign_key => "rhinobook_books_id"
		accepts_nested_attributes_for :chapters, allow_destroy: true, reject_if: :all_blank

		has_many :domains, :dependent => :destroy, :foreign_key => "rhinobook_books_id"
		accepts_nested_attributes_for :domains, allow_destroy: true, reject_if: :all_blank

		has_many :authors, :dependent => :destroy,  :foreign_key => "rhinobook_books_id"
		accepts_nested_attributes_for :authors, allow_destroy: true, reject_if: :all_blank

		has_and_belongs_to_many :libraries, join_table: :rhinobook_libraries_books

		mount_uploader :image_ru, ImageUploader
		mount_uploader :image_en, ImageUploader

		validates :name, :slug, presence: true
		validates_uniqueness_of :name, :slug		

		translates :name, :descr, :author, :active, :show_name, versioning: :paper_trail
		globalize_accessors :locales => I18n.available_locales, :attributes => translated_attribute_names	

		def can_edit?
			res = true
			pages.each do |p|
				if !p.can_edit?
					res = false
					break
				end
			end

			return res
    	end

    	def author
    		authors.where(active: true).first if authors.any?
    	end

		private
			def name_to_slug
				if !self.slug.present?
					self.slug = Rhinoart::Utils.to_slug(self.name)
				else
					self.slug = Rhinoart::Utils.to_slug(self.slug)
				end
			end
	end
end
