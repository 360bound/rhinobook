# == Schema Information
#
# Table name: rhinobook_videos
#
#  id                :integer          not null, primary key
#  videoable_id      :integer
#  videoable_type    :string(255)
#  name              :string(255)
#  file              :string(255)
#  file_content_type :string(255)
#  position          :integer
#  info              :text(65535)
#  created_at        :datetime
#  updated_at        :datetime
#

module Rhinobook
	class Video < ActiveRecord::Base
		belongs_to :videoable, polymorphic: true

		SAFE_INFO_ACCESSORS = [ :poster ]
		store :info, accessors: SAFE_INFO_ACCESSORS, coder: JSON

		validates :file, presence: true	

		mount_uploader :file, VideoUploader

		has_many :images, as: :imageable, :dependent => :destroy
		accepts_nested_attributes_for :images, allow_destroy: true

    # def as_json(options = {})
    #   options[:only] ||= [:name]
    #   options[:methods] ||= [:url, :file_size]
    #   super(options)
    # end
    #
    # def url
    #   file.try(:url)
    # end
    #

    def file_size
      file.try(:size).to_i + images.to_a.sum(&:file_size)
    end

	end
end
