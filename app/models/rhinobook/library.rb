# == Schema Information
#
# Table name: rhinobook_libraries
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  slug        :string(255)      not null
#  domain_name :string(255)
#  descr       :text(65535)
#  active      :boolean          default(TRUE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

module Rhinobook
	class Library < ActiveRecord::Base
		has_and_belongs_to_many :books, join_table: :rhinobook_libraries_books

		validates :name, :slug, presence: true
		validates_uniqueness_of :name, :slug, scope: :locale
	end
end
