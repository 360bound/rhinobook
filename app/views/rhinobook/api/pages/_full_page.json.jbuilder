json.partial! 'rhinobook/api/pages/page', page: page
json.videos page.videos do |video|
  json.partial! 'rhinobook/api/file', file: video
  json.images do
    json.partial! 'rhinobook/api/file', collection: video.images, as: :file
  end
end
json.images do
  json.partial! 'rhinobook/api/file', collection: page.images, as: :file
end