json.partial! 'full_page', page: @page
json.common_images_size @page.images.to_a.sum(&:file_size)
json.common_videos_size @page.videos.to_a.sum(&:file_size)