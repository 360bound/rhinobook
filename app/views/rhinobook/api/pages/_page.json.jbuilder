json.id page.id
json.chapter_id page.rhinobook_chapters_id
json.number page.num
json.content page.content
json.advice page.advice.to_i
json.resume page.resume.to_i