json.partial! 'chapter', chapter: @chapter
json.pages @chapter.pages do |page|
  json.partial! 'rhinobook/api/pages/full_page', page: page
end
json.common_images_size @chapter.pages.to_a.sum{|p| p.images.to_a.sum(&:file_size)}
json.common_videos_size @chapter.pages.to_a.sum{|p| p.videos.to_a.sum(&:file_size)}