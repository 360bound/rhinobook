json.id book.id
json.name book.name
json.slug book.slug
json.descr book.descr
json.created_at format_date(book.created_at)
json.updated_at format_date(book.created_at)
json.author book.author
json.version book.version
json.image do
  json.partial! 'image', image: book.send("image_#{locale}")
end