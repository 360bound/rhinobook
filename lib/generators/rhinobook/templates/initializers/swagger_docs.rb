Swagger::Docs::Config.class_eval do

  register_apis({
      "1.0" => {
          # the extension used for the API
          :api_extension_type => :json,
          # the output location where your .json files are written to
          :api_file_path => 'public/api/swagger',
          # the URL base path to your API
          :base_path => 'http://localhost:3000/api/v1'
      }
  })

  def self.base_application
    Rhinobook::Api::Engine
  end

  def self.transform_path(path, api_version)
    "http://localhost:3000/api/swagger/#{path}"
  end

end