module Rhinobook
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path('../templates', __FILE__)
    desc 'Rhinobook installation'

    def copy_files
      copy_file "initializers/swagger_docs.rb", "config/initializers/swagger_docs.rb"
    end

    def add_route
      route 'mount Rhinobook::Api::Engine, at: "/api"'
      route 'mount Rhinobook::Engine, at: "/admin/library"'
    end

  end
end