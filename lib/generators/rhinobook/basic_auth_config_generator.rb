module Rhinobook
  class BasicAuthConfigGenerator < Rails::Generators::Base
    source_root File.expand_path('../templates', __FILE__)
    desc 'Rhinobook Basic auth config installation'

    def copy_config_file
      copy_file "config/basic_auth.yml", "config/basic_auth.yml"
    end

  end
end