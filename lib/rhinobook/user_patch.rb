module Rhinobook
  module UserPatch
    extend ActiveSupport::Concern

    included do
      class_eval do

        before_save :set_auth_token

        API_ROLE_USER = "Api User"
        API_ROLES = [API_ROLE_USER]

      end
    end

    def set_auth_token
      reset_auth_token if auth_token.blank?
    end

    def reset_auth_token
      self.auth_token = generate_authentication_token
    end

    def reset_auth_token!
      reset_auth_token
      save!
    end

    def has_access_to_api?
      API_ROLES.any?{|r| has_role?(r)}
    end

    private

    def generate_authentication_token
      loop do
        token = Devise.friendly_token
        break token unless self.class.where(auth_token: token).first
      end
    end

  end
end