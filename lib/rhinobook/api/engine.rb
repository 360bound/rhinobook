require "rhinobook/api/engine"
module Rhinobook
  module Api
    class Engine < ::Rails::Engine
      paths["config/routes.rb"] = "config/api_routes.rb"
    end
  end
end