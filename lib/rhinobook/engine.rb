require 'haml-rails'
require 'mini_magick'
require 'carrierwave'
require 'jquery-rails'
require 'acts_as_list'
# require 'rhinoart'
# require "rhinoart/utils"
require 'swagger/docs'
require 'grape-swagger-ui'
require 'rhinobook/user_patch'
require "select2-rails"

module Rhinobook
  class Engine < ::Rails::Engine
    isolate_namespace Rhinobook

    initializer "rhinobook.init" do |app|     

      Rhinoart::User.add_admin_role 'Book Manager'
      Rhinoart::User.add_admin_role 'Book Author'
      Rhinoart::User.add_admin_role 'Book Creator'
      Rhinoart::User.add_admin_role 'Book Editor'
      Rhinoart::User.add_admin_role 'Book Publisher'

      Rhinoart::User.send(:include, Rhinobook::UserPatch)

      Rhinoart::Menu::MainMenu.add_item({
        icon: 'fa-icon-book',
        link: proc{ rhinobook.books_path },
        label: 'rhinoart_gallery.books',
        allowed: proc{ can?(:manage, :books) },
        active: proc{ 
        	controller_name == 'books' ||  
        	controller_name == 'chapters' ||  
        	controller_name == 'domains' ||  
        	controller_name == 'pages' ||  
        	controller_name == 'statuses' ||
          controller_name == 'authors' ||
          controller_name == 'libraries'
        }
      })

    end

    config.to_prepare do
      Devise::SessionsController.layout "application"
    end

    config.sanitize_filter = {
      # :elements => ['p', 'ul', 'li', 'ol', 'table', 'tr', 'td', 'img', 'br', 'hr', 'b', 'strong', 'h2', 'h3', 'h4', 'i', 'pre', 'em', 'video', 'source', 'span'],
      :elements => ['p', 'ul', 'li', 'ol', 'br', 'b', 'strong', 'h2', 'h3', 'h4', 'i', 'pre', 'em', 'video_', 'source_', 'span'],
      :attributes => {
        # 'img' => ['src', 'alt', 'title'],
        'p' => ['style', 'color'],
        'video_' => ['width', 'height', 'controls', 'autoplay', 'loop', 'muted', 'poster', 'preload'],
        'source_' => ['src', 'type'],
        'span' => ['style']
      },
      :css => {
        :properties => ['color', 'text-align'],
      },
    }

  end
end
