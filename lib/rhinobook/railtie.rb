require "rhinobook/ability"
require 'rhinoart' 

module Rhinobook
  class Railtie < Rails::Railtie

    initializer "rhinobook_railtie.configure_rails_initialization" do |app|
      app.config.assets.precompile += %w( 
      	fancybox/*.css 
      	fancybox/*.js
      	jquery.remotipart.js
        swagger_ui.css
        swagger_ui.js  
      )
      app.config.assets.paths << "vendor/assets/javascripts"
      Rhinoart.register_stylesheet 'rhinobook/application.css'
      Rhinoart.register_javascript 'rhinobook/application.js'
    end


    initializer "rhinobook_railtie.configure_ability" do |app|
      ::Ability.send(:include, Rhinobook::Ability)
    end

    initializer "rhinobbok_railtie.swagger_docs_patch" do |app|
      Swagger::Docs::Generator.class_eval do
          def self.transform_spec_to_api_path(spec, controller_base_path, extension)
            api_path = spec.to_s.dup
            api_path.gsub!('(.:format)', extension ? ".#{extension}" : '')
            api_path.gsub!(/:(\w+)/, '{\1}')
            api_path.gsub!(controller_base_path, '')
            api_path.gsub!(/\((.+)\)/, '\1')
            trim_slashes(api_path)
          end
      end
    end

  end
end

