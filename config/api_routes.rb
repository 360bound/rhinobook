Rhinobook::Api::Engine.routes.draw do
  namespace :rhinobook, :path => '/' do
    namespace :api, :path => '/', :format => :json do
      scope :v1 do
        scope "(:locale)", locale: /ru|en/ do
          get 'docs' => 'documentation#show'
          resources :books, only: [:index, :show] do
            get :version, :on => :member
            resources :chapters, only: [:index]
          end
          resources :chapters, only: [:show] do
            resources :pages, only: [:index]
          end
          resources :pages, only: [:show]
          resources :users, only: [:create, :show] do
            post :authorize, on: :collection
          end
        end
      end
    end
  end
end